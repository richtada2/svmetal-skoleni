package com.example.seznam.models;

import android.annotation.SuppressLint;

import java.util.Locale;

public class Item {

    private String name;
    private double price;
    private int amount;

    public Item()
    {

    }

    public Item( String name, double price, int amount)
    {
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    public String getName(){
        return this.name;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public void addTen()
    {
        this.amount = this.amount + 10;
    }

    public double getPrice() {
        return price;
    }

    public String getFormattedPrice(){
        return String.format("%.2f czk", price);
    }

    public int getAmount() {
        return amount;
    }
}
