package com.example.seznam.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.seznam.R;
import com.example.seznam.models.Fruit;
import com.example.seznam.models.Item;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        TextView itemTitle = findViewById(R.id.am_item_title);
        ImageView itemImage = findViewById(R.id.am_item_image);
        TextView itemPrice = findViewById(R.id.am_item_price);

        Item bozkov = new Item("Tuzemák", 60, 10);

        itemTitle.setText(bozkov.getName());
        itemImage.setBackgroundResource(R.drawable.ic_baseline_cruelty_free_24);
        itemPrice.setText(bozkov.getFormattedPrice());








        Item apple = new Item("jablko", 10, 100);

        Item rum = new Item("Božkov", 100, 10);
        Item rums = new Item("Basa rumu", 600, 1);


        Fruit hruska = new Fruit("hruska super gold", 10, 100);


        ArrayList<Item> items = new ArrayList<>();
        items.add(bozkov);
        items.add(rum);
        items.add(hruska);

        items.clear();

        for (int i =0; i < items.size(); i++){
            items.get(i);
        }

        String rumName = rum.getName();
        rum.setAmount(100);

        addItem(hruska);
        addItem(bozkov);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void addItem(Item i)
    {

    }

    private void addItem(Fruit i)
    {

    }
}